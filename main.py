import requests


API_KEY = 'trnsl.1.1.20190712T081241Z.0309348472c8719d.0efdbc7ba1c507292080e3fbffe4427f7ce9a9f0'
URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate'


def translate(from_file, to_file, to_lang = 'ru'):
    
    #lang = language + '-{}'
    #document = language.upper() + '.txt'
    
    doc = open(from_file,'r')
    params = { 
        'key': API_KEY,
        'text': doc,
        'lang': format(to_lang),
    }

    response = requests.get(URL, params=params)
    json_ = response.json()
    
    #res = 'res_' + document

    f = open(to_file, 'w')
    for symb in ''.join(json_['text']):
        f.write(symb)
    f.close()
    pass

translate('DE.txt', 'res_DE.txt')
translate('ES.txt', 'res_ES.txt')
translate('FR.txt', 'res_FR.txt')
